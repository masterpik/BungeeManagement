package com.masterpik.bungeemanagement.server.close;

import com.masterpik.api.Networks.Servers;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;

public class CloseServerManagement {

    public static ArrayList<String> closed;

    public static String serverClosed1;
    public static String serverClosed2;

    public static String prefix;
    public static String help;

    public static void CloseServerInit() {
        serverClosed1 = Api.getString("general.masterpik.main.logoTextChat")+" "+Api.getString("proxy.errors.serverClosed.1");
        serverClosed2 = Api.getString("proxy.errors.serverClosed.2");

        prefix = "ServersManagement";

        help = prefix+" list of commands : \n" +
                " /srv close [server]\n" +
                " /srv open [server]\n" +
                " /srv list\n" +
                " /srv kick [server] [toHub]\n" +
                " /srv help [command]";

        closed = new ArrayList<>();
    }


    public static boolean closeServer(String server) {
        if (!closed.contains(server)) {
            closed.add(server);
            return true;
        } else {
            return false;
        }
    }

    public static boolean openServer(String server) {
        if (closed.contains(server)) {
            closed.remove(server);
            return true;
        } else {
            return false;
        }
    }

    public static void kickAll(String server, boolean hub) {
        ArrayList<ProxiedPlayer> players = new ArrayList<>();
        players.addAll(ProxyServer.getInstance().getServers().get(server).getPlayers());

        if (!UtilNetwork.serverIsOnline(Servers.HUB)) {
            hub = false;
        }

        if (hub) {
            for (ProxiedPlayer player : players) {
                player.connect(ProxyServer.getInstance().getServers().get("hub"));
            }
        } else {
            for (ProxiedPlayer player : players) {
                player.disconnect(new TextComponent(CloseServerManagement.serverClosed1 + server + CloseServerManagement.serverClosed2));
            }
        }

    }

}

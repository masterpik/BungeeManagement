package com.masterpik.bungeemanagement.server.close;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.database.UUIDName;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.UUID;

public class CommandsCloseServer extends Command{


    public CommandsCloseServer(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        BungeeLogger log = new BungeeLogger(sender, Colors.DARK_RED);

        String prefix = CloseServerManagement.prefix;

        if (strings != null && strings.length >= 1) {
            if (strings.length >= 2
                    && (strings[0].equalsIgnoreCase("close")
                    || strings[0].equalsIgnoreCase("open")
                    || strings[0].equalsIgnoreCase("kick"))) {

                String server = strings[1];
                server = server.toLowerCase();

                if (ProxyServer.getInstance().getServers().containsKey(server)) {

                    if (strings[0].equalsIgnoreCase("close")) {
                        if (CloseServerManagement.closeServer(server)) {
                            log.info(prefix+" the server "+strings[1]+" has been closed");
                        } else {
                            log.info(prefix+" the server "+strings[1]+" is already closed");
                        }
                    } else if (strings[0].equalsIgnoreCase("open")) {
                        if (CloseServerManagement.openServer(server)) {
                            log.info(prefix+" the server "+strings[1]+" has been opened");
                        } else {
                            log.info(prefix+" the server "+strings[1]+" is already opened");
                        }
                    } else if (strings[0].equalsIgnoreCase("kick")) {

                        boolean toHub = true;

                        if (strings.length >= 3){

                            toHub = Boolean.parseBoolean(strings[2]);

                        }

                        if (server.equalsIgnoreCase("hub")){
                            toHub = false;
                        }

                        CloseServerManagement.kickAll(server, toHub);
                        log.info(prefix +" kick all player from the server "+strings[1]+" to hub : "+Boolean.toString(toHub));
                    }

                } else {
                    log.info(prefix+" the server "+strings[1]+" is not register");
                }


            }
            else if (strings.length >=1
                    && strings[0].equalsIgnoreCase("list")) {

                log.info(prefix+" list of servers : ");

                for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
                    log.info(" - "+server.getName()+" isOpen : "+!(CloseServerManagement.closed.contains(server.getName())));
                }

            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                    if (strings[1].equalsIgnoreCase("close")) {
                        log.info(prefix+" help (close) : \n Description : Close a server\n Usage : /srv close [server]");
                    } else if (strings[1].equalsIgnoreCase("open")) {
                        log.info(prefix+" help (open) : \n Description : Open a server\n Usage : /srv open [server]");
                    } else if (strings[1].equalsIgnoreCase("kick")) {
                        log.info(prefix+" help (kick) : \n Description : Kick all players from a server \n Usage : /srv kick [server] [toHub]");
                    } else if (strings[1].equalsIgnoreCase("list")) {
                        log.info(prefix+" help (list) : \n Description : Get list of servers \n Usage : /srv list");
                    } else if (strings[1].equalsIgnoreCase("help")) {
                        log.info(prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /srv help [command]");
                    } else {
                        log.info(CloseServerManagement.help);
                    }

                } else {
                    log.info(CloseServerManagement.help);
                }
            } else {
                log.info(CloseServerManagement.help);
            }
        } else {
            log.info(CloseServerManagement.help);
        }


    }
}

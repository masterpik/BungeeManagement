package com.masterpik.bungeemanagement;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.api.util.UtilString;
import com.masterpik.bungeemanagement.ban.ip.BanIp;
import com.masterpik.bungeemanagement.ban.ip.BanIpManagement;
import com.masterpik.bungeemanagement.ban.reason.ReasonManagement;
import com.masterpik.bungeemanagement.ban.uuid.BanUUID;
import com.masterpik.bungeemanagement.ban.uuid.BanUUIDManagement;
import com.masterpik.bungeemanagement.commands.CommandsPerms;
import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.bungeemanagement.maintenance.MaintenanceManagement;
import com.masterpik.bungeemanagement.mute.MuteManagement;
import com.masterpik.bungeemanagement.ping.playersinfo.PlayersInfoManagement;
import com.masterpik.bungeemanagement.server.close.CloseServerManagement;
import com.masterpik.bungeemanagement.whitelist.ip.WhitelistIpManagement;
import com.masterpik.bungeemanagement.whitelist.uuid.WhitelistUUIDManagement;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.*;

public class Events implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void ServerConnectEvent(ServerConnectEvent event) {

        if (event.getPlayer().getServer() == null) {

            ProxiedPlayer player = event.getPlayer();

            String stringIp = player.getAddress().toString().split(":")[0];
            stringIp = stringIp.replaceAll("/", "");
            UUID uuid = player.getUniqueId();

            if (ProxyServer.getInstance().getPlayers().size()-1 >= PlayersInfoManagement.maxPlayers) {
                //TODO work with status
                if (!WhitelistUUIDManagement.isWhitelist(uuid)
                        && !WhitelistIpManagement.isWhitelistIp(stringIp)) {
                    player.disconnect(new TextComponent(PlayersInfoManagement.kick));
                    event.setCancelled(true);
                }
            } else {

                if (GeneralConfy.isMainteance()) {
                    if (GeneralConfy.isWhitelistIp()
                            && !GeneralConfy.isWhitelistUUID()) {
                        if (!WhitelistIpManagement.isWhitelist(stringIp)) {
                            player.disconnect(new TextComponent(MaintenanceManagement.kick));
                            event.setCancelled(true);
                        }
                    } else if (!GeneralConfy.isWhitelistIp()
                            && GeneralConfy.isWhitelistUUID()) {
                        if (!WhitelistUUIDManagement.isWhitelist(player.getUniqueId())) {
                            player.disconnect(new TextComponent(MaintenanceManagement.kick));
                            event.setCancelled(true);
                        }
                    } else if (GeneralConfy.isWhitelistIp()
                            && GeneralConfy.isWhitelistUUID()) {
                        if (!WhitelistIpManagement.isWhitelist(stringIp)
                                && !WhitelistUUIDManagement.isWhitelist(player.getUniqueId())) {
                            player.disconnect(new TextComponent(MaintenanceManagement.kick));
                            event.setCancelled(true);
                        }
                    } else if (!GeneralConfy.isWhitelistIp()
                            && !GeneralConfy.isWhitelistUUID()) {
                        player.disconnect(new TextComponent(MaintenanceManagement.kick));
                        event.setCancelled(true);
                    }
                } else {

                    if (BanIpManagement.isBan(stringIp)) {
                        BanIp ban = BanIpManagement.getBanInfo(stringIp);
                        String kik = BanIpManagement.kick;
                        kik = kik.replaceAll("%t", ban.getEndDateStringFrench());
                        kik = kik.replaceAll("%r", ReasonManagement.getReason(ban.getReason()));
                        player.disconnect(new TextComponent(kik));
                        event.setCancelled(true);
                    } else if (BanUUIDManagement.isBan(uuid)) {
                        BanUUID ban = BanUUIDManagement.getBanInfo(uuid);
                        String kik = BanUUIDManagement.kick;
                        kik = kik.replaceAll("%t", ban.getEndDateStringFrench());
                        kik = kik.replaceAll("%r", ReasonManagement.getReason(ban.getReason()));
                        player.disconnect(new TextComponent(kik));
                        event.setCancelled(true);
                    } else if (GeneralConfy.isWhitelistIp() && !WhitelistIpManagement.isWhitelist(stringIp)) {
                        player.disconnect(new TextComponent(WhitelistIpManagement.kick));
                        event.setCancelled(true);
                    } else if (GeneralConfy.isWhitelistUUID() && !WhitelistUUIDManagement.isWhitelist(uuid)) {
                        player.disconnect(new TextComponent(WhitelistUUIDManagement.kick));
                        event.setCancelled(true);
                    }
                }
            }
        }




    }



    @EventHandler (priority = EventPriority.LOWEST)
    public void ChatEvent(ChatEvent event) {

        String message = event.getMessage();

        if (event.getSender() instanceof ProxiedPlayer) {

            ProxiedPlayer player = (ProxiedPlayer) event.getSender();

            if (message.toCharArray()[0] == '/') {

                String command = message.split(" ")[0];
                command = command.toLowerCase();

                /*boolean haveRight = true;

                if (CommandsPerms.permissions.containsKey(player.getUniqueId().toString())) {
                    if (!CommandsPerms.permissions.get(player.getUniqueId().toString()).contains("*")
                            && !CommandsPerms.permissions.get(player.getUniqueId().toString()).contains(command)
                            && !CommandsPerms.permissions.get("*").contains("*")
                            && !CommandsPerms.permissions.get("*").contains(command)) {
                        haveRight = false;
                    }
                } else {
                    if (!CommandsPerms.permissions.get("*").contains("*")
                            && !CommandsPerms.permissions.get("*").contains(command)) {
                        haveRight = false;
                    }
                }*/

                if (!CommandsPerms.hasPermission(player.getUniqueId().toString(), command)) {
                    event.setCancelled(true);
                    player.sendMessage(new TextComponent(CommandsPerms.noPermission));
                }



            } else {

                if (MuteManagement.isMute(player.getUniqueId())) {
                    player.sendMessage(new TextComponent(MuteManagement.getMuteMsg(MuteManagement.getMuteInfo(player.getUniqueId()))));
                    event.setCancelled(true);
                }

            }
        }
    }



    @EventHandler
    public void PluginMessageEvent(PluginMessageEvent event) {


        if (event.getTag().equals("BungeeCord")) {
            ProxiedPlayer player = null;
            String playerName = "null";

            ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());

            String type = in.readUTF();
            String server = in.readUTF();

            if (type.equalsIgnoreCase("Connect")
                    && event.getReceiver() instanceof ProxiedPlayer) {
                player = (ProxiedPlayer) event.getReceiver();
                playerName = player.getName();
            } else if (type.equalsIgnoreCase("Forward")) {
                String subChannel = in.readUTF();

                if (subChannel.equalsIgnoreCase("gamesCo")) {
                    String getMessage = in.readUTF();

                    ArrayList<String> data = UtilString.CryptedStringToArrayList(getMessage);

                    player = ProxyServer.getInstance().getPlayer(data.get(1));
                    if (player != null) {
                        playerName = player.getName();
                    }

                }
            }

            if (!type.equalsIgnoreCase("Forward")) {
                ProxyServer.getInstance().getLogger().info("Plugin message BungeeCord : | type : " + type + " | to : " + server + " | player : " + playerName);
            }

            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(server);
            String serverIp = serverInfo.getAddress().getHostName();
            int serverPort = serverInfo.getAddress().getPort();

            boolean cancel = false;

            if (!ProxyServer.getInstance().getServers().containsKey(server)
                    || !UtilNetwork.serverIsOnline(serverIp, serverPort)){
                cancel = true;
            } else {
                if (CloseServerManagement.closed.contains(server)) {
                    cancel = player == null || !(CommandsPerms.hasPermission(player.getUniqueId().toString(), "0server.*") || CommandsPerms.hasPermission(player.getUniqueId().toString(), "0server." + server));
                }
            }





            if (cancel) {
                event.setCancelled(true);
                if (!type.equalsIgnoreCase("Forward")) {
                    ProxyServer.getInstance().getLogger().info("message cancelled");
                }

        /*ArrayList<Servers> servers = new ArrayList<>(Arrays.asList(Servers.values()));

        servers.removeIf(p -> (p.getName().equalsIgnoreCase(server)));

        ProxyServer.getInstance().getLogger().info("server ip by api : " + servers.get(0).getIp());*/

                if (player != null) {

                    if (type.equalsIgnoreCase("Connect")) {
                        player.sendMessage(new TextComponent(CloseServerManagement.serverClosed1 + server + CloseServerManagement.serverClosed2));
                    }
                }
            }


        }
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onPing(ProxyPingEvent event) {
        ServerPing.Players playersInfo = event.getResponse().getPlayers();
        event.getResponse().setPlayers(new ServerPing.Players(PlayersInfoManagement.maxPlayers, playersInfo.getOnline(), playersInfo.getSample()));
    }

}

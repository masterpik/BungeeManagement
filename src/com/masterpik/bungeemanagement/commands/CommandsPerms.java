package com.masterpik.bungeemanagement.commands;

import com.masterpik.bungeecommands.aliases.Aliases;
import com.masterpik.bungeemanagement.confy.CommandsPermsConfy;
import com.masterpik.messages.bungee.Api;

import java.util.ArrayList;
import java.util.HashMap;

public class CommandsPerms {
    public static HashMap<String, ArrayList<String>> permissions;
    public static String noPermission;

    public static String prefix;

    public static String help;

    public static void CommandsPermsInit() {
        permissions = new HashMap<>();

        prefix = "Permissions";

        noPermission = Api.getString("general.masterpik.main.logoTextChat")+" "+Api.getString("proxy.commands.noPerm");

        help = prefix+" list of commands : \n" +
                " /perms add [uuid|player] [command]\n" +
                " /perms remove [uuid|player] [command]\n" +
                " /perms getPerms [uuid|player]\n" +
                " /perms help [command]";
    }

    public static void getPermissions() {
        permissions = CommandsPermsConfy.getHashMap();
    }

    public static boolean hasPermission(String player, String cmd) {

        String command = cmd.split(" ")[0];
        command = command.toLowerCase();

        command = "/"+Aliases.getCommandFromAlias(command.replaceFirst("/", ""));

        boolean haveRight = true;

        if (CommandsPerms.permissions.containsKey(player)) {
            if (!CommandsPerms.permissions.get(player).contains("*")
                    && !CommandsPerms.permissions.get(player).contains(command)
                    && !CommandsPerms.permissions.get("*").contains("*")
                    && !CommandsPerms.permissions.get("*").contains(command)) {
                haveRight = false;
            }
        } else {
            if (!CommandsPerms.permissions.get("*").contains("*")
                    && !CommandsPerms.permissions.get("*").contains(command)) {
                haveRight = false;
            }
        }

        return haveRight;
    }

}

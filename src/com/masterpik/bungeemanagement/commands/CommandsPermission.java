package com.masterpik.bungeemanagement.commands;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeemanagement.confy.CommandsPermsConfy;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.sql.Date;
import java.util.ArrayList;
import java.util.UUID;

public class CommandsPermission extends Command {
    public CommandsPermission(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        BungeeLogger log = new BungeeLogger(sender, Colors.YELLOW);

        String prefix = CommandsPerms.prefix;

        if (strings != null && strings.length >= 1) {
            if (strings.length >= 3
                    && (strings[0].equalsIgnoreCase("add")
                    || strings[0].equalsIgnoreCase("remove"))) {

                String uuid = null;

                if (UtilUUID.isAnUUID(strings[1])) {
                    uuid = UUID.fromString(strings[1]).toString();
                } else if (UUIDName.playerNameToUUID(strings[1]) != null) {
                    uuid = UUIDName.playerNameToUUID(strings[1]).toString();
                } else if (strings[1].equalsIgnoreCase("*")) {
                    uuid = "*";
                }

                String command = strings[2].toLowerCase();

                if (uuid != null
                        && (command.toCharArray()[0] == '/' || command.toCharArray()[0] == '*' || command.toCharArray()[0] == '0')) {

                    if (strings[0].equalsIgnoreCase("add")) {

                        boolean addBool = true;
                        if (CommandsPerms.permissions.containsKey(uuid)
                                && CommandsPerms.permissions.get(uuid).contains(command)) {
                            addBool = false;
                        }

                        if (addBool) {
                            CommandsPermsConfy.addCommand(uuid, command);
                            log.info(prefix+" add permission for command : "+strings[2]+" to player : "+strings[1]);
                        } else {
                            log.info(prefix+" can't add permission for command : "+strings[2]+" to player : "+strings[1]);
                        }
                    } else if (strings[0].equalsIgnoreCase("remove")) {

                        boolean rmBool = false;
                        if (CommandsPerms.permissions.containsKey(uuid)
                                && CommandsPerms.permissions.get(uuid).contains(command)) {
                            rmBool = true;
                        }


                        if (rmBool) {
                            CommandsPermsConfy.removeCommand(uuid, command);
                            log.info(prefix+" remove permission for command : "+strings[2]+" to player : "+strings[1]);
                        } else {
                            log.info(prefix+" can't remove permission for command : "+strings[2]+" to player : "+strings[1]);
                        }
                    }

                } else {
                    log.info(prefix+" format error on uuid|player : "+strings[1]+" or on command : "+strings[2]+" (ps Don't miss the \"/\" before the command)");
                }

            }
            else if (strings.length >=2
                    && strings[0].equalsIgnoreCase("getPerms")) {

                String uuid = null;

                if (UtilUUID.isAnUUID(strings[1])) {
                    uuid = UUID.fromString(strings[1]).toString();
                } else if (UUIDName.playerNameToUUID(strings[1]) != null) {
                    uuid = UUIDName.playerNameToUUID(strings[1]).toString();
                } else if (strings[1].equalsIgnoreCase("*")) {
                    uuid = "*";
                }

                if (uuid != null) {

                    if (strings[0].equalsIgnoreCase("getPerms")) {

                        if (!CommandsPerms.permissions.containsKey(uuid)) {
                            uuid = "*";
                        }

                        String info= "";

                        ArrayList<String> cmds = new ArrayList<>();
                        cmds.addAll(CommandsPerms.permissions.get(uuid));

                        if (!cmds.contains("*")
                                && !uuid.equalsIgnoreCase("*")) {
                            cmds.addAll(CommandsPerms.permissions.get("*"));
                        }

                        for (String str : cmds) {
                            info = info + "\n - "+str;
                        }

                        log.info(prefix+" permissions for player : "+ strings[1]+" : "+info);
                    }

                } else {
                    log.info(prefix+ " format error on uuid|player : "+strings[1]);
                }

            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                    if (strings[1].equalsIgnoreCase("add")) {
                        log.info(prefix+" help (add) : \n Description : Add command permission for a uuid\n Usage : /perms add [uuid|player] [command]");
                    } else if (strings[1].equalsIgnoreCase("remove")) {
                        log.info(prefix+" help (remove) : \n Description : Remove command permission for a uuid\n Usage : /perms remove [uuid|player] [command]");
                    } else if (strings[1].equalsIgnoreCase("getPerms")) {
                        log.info(prefix+" help (getPerms) : \n Description : Get list of permissions commands for a uuid \n Usage : /perms getPerms [uuid|player]");
                    } else if (strings[1].equalsIgnoreCase("help")) {
                        log.info(prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /perms help [command]");
                    } else {
                        log.info(CommandsPerms.help);
                    }

                } else {
                    log.info(CommandsPerms.help);
                }
            } else {
                log.info(CommandsPerms.help);
            }
        } else {
            log.info(CommandsPerms.help);
        }


    }
}

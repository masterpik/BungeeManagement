package com.masterpik.bungeemanagement;

import com.masterpik.bungeemanagement.ban.ip.BanIpManagement;
import com.masterpik.bungeemanagement.ban.ip.CommandsBanIp;
import com.masterpik.bungeemanagement.ban.reason.ReasonManagement;
import com.masterpik.bungeemanagement.ban.uuid.BanUUIDManagement;
import com.masterpik.bungeemanagement.ban.uuid.CommandsBanUUID;
import com.masterpik.bungeemanagement.commands.CommandsPermission;
import com.masterpik.bungeemanagement.commands.CommandsPerms;
import com.masterpik.bungeemanagement.confy.CommandsPermsConfy;
import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.bungeemanagement.maintenance.CommandsMaintenance;
import com.masterpik.bungeemanagement.maintenance.MaintenanceManagement;
import com.masterpik.bungeemanagement.mute.CommandsMute;
import com.masterpik.bungeemanagement.mute.MuteManagement;
import com.masterpik.bungeemanagement.ping.playersinfo.CommandsPlayersInfo;
import com.masterpik.bungeemanagement.ping.playersinfo.PlayersInfoManagement;
import com.masterpik.bungeemanagement.server.close.CloseServerManagement;
import com.masterpik.bungeemanagement.server.close.CommandsCloseServer;
import com.masterpik.bungeemanagement.whitelist.ip.CommandsWhitelistIp;
import com.masterpik.bungeemanagement.whitelist.ip.WhitelistIpManagement;
import com.masterpik.bungeemanagement.whitelist.uuid.CommandsWhitelistUUID;
import com.masterpik.bungeemanagement.whitelist.uuid.WhitelistUUIDManagement;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.sql.Connection;

public class Main extends Plugin {

    public static Plugin plugin;
    public static Connection dbConnection;

    @Override
    public void onEnable() {
        plugin = this;

        dbConnection = com.masterpik.database.bungee.Main.dbConnection;

        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();

        pluginManager.registerListener(this, new Events());

        pluginManager.registerCommand(this, new CommandsWhitelistIp("wip"));
        pluginManager.registerCommand(this, new CommandsWhitelistUUID("wuuid"));

        pluginManager.registerCommand(this, new CommandsBanIp("banip"));
        pluginManager.registerCommand(this, new CommandsBanUUID("banuuid"));

        pluginManager.registerCommand(this, new CommandsMute("mute"));

        pluginManager.registerCommand(this, new CommandsMaintenance("maintenance"));

        pluginManager.registerCommand(this, new CommandsPermission("perms"));
        pluginManager.registerCommand(this, new CommandsPermission("perm"));

        pluginManager.registerCommand(this, new CommandsCloseServer("srv"));

        pluginManager.registerCommand(this, new CommandsPlayersInfo("bping"));

        GeneralConfy.GeneralInit();
        CommandsPermsConfy.CommandsPermsInit();
        CommandsPerms.CommandsPermsInit();
        CommandsPerms.getPermissions();

        ReasonManagement.reasonInit();
        BanIpManagement.BanIpInit();
        BanUUIDManagement.BanUUIDInit();

        com.masterpik.bungeemanagement.mute.reason.ReasonManagement.reasonInit();
        MuteManagement.MuteInit();

        WhitelistIpManagement.WhitelistIpInit();
        WhitelistUUIDManagement.WhitelistUUIDInit();

        MaintenanceManagement.MaintenanceInit();

        CloseServerManagement.CloseServerInit();

        PlayersInfoManagement.playersInfoInit();

    }

}

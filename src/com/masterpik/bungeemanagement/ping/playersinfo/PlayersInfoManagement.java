package com.masterpik.bungeemanagement.ping.playersinfo;

import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.messages.bungee.Api;

public class PlayersInfoManagement {

    public static String kick;
    public static int maxPlayers;

    public static String prefix;
    public static String help;

    public static void playersInfoInit() {
        kick = Api.getString("general.masterpik.main.logoText")+"\n \n"+Api.getString("proxy.errors.maxPlayers");
        maxPlayers = GeneralConfy.getMaxPlayers();

        prefix = "PingManagement";

        help = prefix+" list of commands : \n" +
                " /bping setMax [max]\n" +
                " /bping getMax\n" +
                " /bping getOnline\n" +
                " /bping getRegister\n" +
                " /bping help [command]";
    }

    public static void setMax(int max) {
        maxPlayers = max;
        GeneralConfy.setMaxPlayers(max);
    }

}

package com.masterpik.bungeemanagement.ping.playersinfo;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.bungeeconnect.database.UUIDName;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Command;

public class CommandsPlayersInfo extends Command {
    public CommandsPlayersInfo(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        BungeeLogger log = new BungeeLogger(sender, Colors.WHITE);

        String prefix = PlayersInfoManagement.prefix;

        if (strings != null && strings.length >= 1) {
            if (strings.length >= 2
                    && (strings[0].equalsIgnoreCase("setMax"))) {

                int max = -1;

                try {
                    max = Integer.parseInt(strings[1]);
                } catch (NumberFormatException e) {
                    max = -1;
                }

                if (max != -1) {

                    PlayersInfoManagement.setMax(max);
                    log.info(prefix+" ping max players set to "+strings[1]+" with sucess");

                } else {
                    log.info(prefix+" format errors on number "+strings[1]);
                }


            }
            else if (strings.length >=1
                    && (strings[0].equalsIgnoreCase("getMax")
                    || strings[0].equalsIgnoreCase("getOnline")
                    || strings[0].equalsIgnoreCase("getRegister"))) {

                if (strings[0].equalsIgnoreCase("getMax")) {
                    log.info(prefix+" ping max players is to "+PlayersInfoManagement.maxPlayers);
                } else if (strings[0].equalsIgnoreCase("getOnline")) {
                    int online = 0;
                    if (!ProxyServer.getInstance().getPlayers().isEmpty()) {
                        online = ProxyServer.getInstance().getPlayers().size();
                    }
                    log.info(prefix+" they have "+Integer.toString(online)+" players online");
                }  else if (strings[0].equalsIgnoreCase("getRegister")) {
                    int register = UUIDName.uuidTable.size();
                    log.info(prefix+" they have "+Integer.toString(register)+" players register");
                }

            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                    if (strings[1].equalsIgnoreCase("setMax")) {
                        log.info(prefix+" help (setMax) : \n Description : Set ping max players\n Usage : /bping setMax [max]");
                    } else if (strings[1].equalsIgnoreCase("getMax")) {
                        log.info(prefix+" help (getMax) : \n Description : Get ping max players\n Usage : /bping getMax");
                    } else if (strings[1].equalsIgnoreCase("getOnline")) {
                        log.info(prefix+" help (getOnline) : \n Description : Get online players\n Usage : /bping getOnline");
                    } else if (strings[1].equalsIgnoreCase("getRegister")) {
                        log.info(prefix+" help (getRegister) : \n Description : Get players register\n Usage : /bping getRegister");
                    } else if (strings[1].equalsIgnoreCase("help")) {
                        log.info(prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /bping help [command]");
                    } else {
                        log.info(PlayersInfoManagement.help);
                    }

                } else {
                    log.info(PlayersInfoManagement.help);
                }
            } else {
                log.info(PlayersInfoManagement.help);
            }
        } else {
            log.info(PlayersInfoManagement.help);
        }
    }
}

package com.masterpik.bungeemanagement.ban.ip;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.bungeeconnect.database.IpPlayers;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.bungeemanagement.whitelist.ip.WhitelistIpManagement;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

public class CommandsBanIp extends Command {
    public CommandsBanIp(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        BungeeLogger log = new BungeeLogger(sender, Colors.AQUA);

        String prefix = BanIpManagement.prefix;

        if (strings != null && strings.length >= 1) {
            if (strings.length >= 4
                    && (strings[0].equalsIgnoreCase("add")
                    || strings[0].equalsIgnoreCase("modif"))) {

                String ip = strings[1];

                boolean inf = false;
                Date date = UtilDate.getCurrentDate();

                if (strings[2].equalsIgnoreCase("inf")) {
                    inf = true;
                    date = Date.valueOf("3000-01-01");
                } else {
                    inf = false;
                    try {
                        date = Date.valueOf(strings[2]);
                    } catch(IllegalArgumentException e) {
                        try {

                            String type = "D";

                            if (strings[2].split("\\.").length >= 2) {

                                if (strings[0].equalsIgnoreCase("modif") && BanIpManagement.getBanInfo(ip) != null) {
                                    date = BanIpManagement.getBanInfo(ip).getEndDate();
                                } else {
                                    date = UtilDate.getCurrentDate();
                                }

                                int nb = Integer.parseInt(strings[2].split("\\.")[0]);
                                type = strings[2].split("\\.")[1];

                                if (UtilDate.getCalendarTypeFromString(type) != -1) {

                                    date = UtilDate.addToDate(date, UtilDate.getCalendarTypeFromString(type), nb);

                                } else {
                                    date = null;
                                }
                            }



                        } catch (NumberFormatException e2) {
                            date = null;
                        }
                    }
                }

                int reason = -1;

                try {
                    reason = Integer.parseInt(strings[3]);
                } catch (NumberFormatException e) {
                    reason = -1;
                }




                if (UtilNetwork.checkIp(ip)
                        && date != null
                        && reason != -1) {

                    BanIp ban = new BanIp(ip, inf, date, reason);

                    if (strings[0].equalsIgnoreCase("add")) {
                        if (BanIpManagement.addBan(ban)) {
                            log.info(prefix+" add ban for ip : "+ip+" "+ban.toStringNoIp());
                        } else {
                            log.info(prefix+" can't add ban for ip : "+ip+" "+ban.toStringNoIp());
                        }
                    } else if (strings[0].equalsIgnoreCase("modif")) {
                        if (BanIpManagement.modifiyBan(ip, ban)) {
                            log.info(prefix+" modify ban for ip : "+ip+" "+ban.toStringNoIp());
                        } else {
                            log.info(prefix+" can't modify ban for ip : "+ip+" "+ban.toStringNoIp());
                        }
                    }

                } else {
                    log.info(prefix+" format error on one of this : (ip : "+ip+" | date : "+strings[2]+ " | reason : "+reason+")");
                }

            }
            else if (strings.length >=2
                    && (strings[0].equalsIgnoreCase("remove") || strings[0].equalsIgnoreCase("isBanIp"))) {

                String ip = strings[1];

                if (UtilNetwork.checkIp(ip)) {

                    if (strings[0].equalsIgnoreCase("remove")) {
                        if (BanIpManagement.removeBan(ip)) {
                            log.info(prefix+" remove ban for ip : "+ ip);
                        } else {
                            log.info(prefix+ " can't remove ban for ip : "+ ip);
                        }
                    } else if (strings[0].equalsIgnoreCase("isBanIp")) {
                        String info = "";

                        if (BanIpManagement.isBan(ip)) {
                            BanIp ban = BanIpManagement.getBanInfo(ip);
                            info = " " +ban.toStringNoIp();
                        }

                        log.info(prefix+" isBanIp "+ ip+" : "+Boolean.toString(BanIpManagement.isBan(ip))+info);
                    }

                } else {
                    log.info(prefix+ " format error on ip : "+ip);
                }

            }
            else if (strings[0].equalsIgnoreCase("list")) {

                String str = prefix+" list : \n";
                //log.info("whitelistIp list : ");

                ArrayList<String> bans = new ArrayList<>();
                bans.addAll(BanIpManagement.getList());

                int bucle1 = 0;
                while (bucle1 < bans.size()) {

                    BanIp ban= BanIpManagement.getBanInfo(bans.get(bucle1));

                    str = str + "  - "+ban.toString()+"\n";

                    bucle1++;
                }

                log.info(str);

            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                    if (strings[1].equalsIgnoreCase("add")) {
                        log.info(prefix+" help (add) : \n Description : Ban an ip\n Usage : /banip add [ip] [days|date|inf] [reason]");
                    } else if (strings[1].equalsIgnoreCase("remove")) {
                        log.info(prefix+" help (remove) : \n Description : Remove ban of an ip\n Usage : /banip remove [ip]");
                    } else if (strings[1].equalsIgnoreCase("modif")) {
                        log.info(prefix+" help (modif) : \n Description : Modify an existant ban\n Usage : /banip modif [ip] [days|date|inf] [reason]");
                    } else if (strings[1].equalsIgnoreCase("isBanIp")) {
                        log.info(prefix+" help (isBanIp) : \n Description : Check if an ip is ban \n Usage : /banip isBanIp [ip]");
                    } else if (strings[1].equalsIgnoreCase("help")) {
                        log.info(prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /banip help [command]");
                    } else {
                        log.info(BanIpManagement.help);
                    }

                } else {
                    log.info(BanIpManagement.help);
                }
            } else {
                log.info(BanIpManagement.help);
            }
        } else {
            log.info(BanIpManagement.help);
        }

    }
}

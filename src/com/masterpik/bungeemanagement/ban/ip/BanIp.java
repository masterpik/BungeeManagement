package com.masterpik.bungeemanagement.ban.ip;

import com.masterpik.api.util.UtilDate;
import com.masterpik.bungeeconnect.database.IpPlayers;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BanIp {

    private String ip;
    private boolean isInfiny;

    private Date endDate;

    private int reason;

    public BanIp(String ip, boolean isInfiny, Date endDate, int reason) {
        this.ip = ip;
        this.isInfiny = isInfiny;
        this.endDate = endDate;
        this.reason = reason;
    }

    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    public boolean isInfiny() {
        return isInfiny;
    }
    public void setIsInfiny(boolean isInfiny) {
        this.isInfiny = isInfiny;
    }

    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getReason() {
        return reason;
    }
    public void setReason(int reason) {
        this.reason = reason;
    }

    public String toString() {
        String info = "";

        String dat = "";
        if (this.isInfiny()) {
            dat = " INFINITY ";
        } else {
            dat = " "+this.endDate.toString();
        }

        info = "(ip : "+this.ip+" ("+ IpPlayers.getPlayersFromIp(this.ip)+") | endDate : "+dat+" | reason : "+Integer.toString(this.getReason())+")";

        return info;
    }

    public String toStringNoIp() {
        String info = "";

        String dat = "";
        if (this.isInfiny()) {
            dat = " INFINITY ";
        } else {
            dat = " "+this.endDate.toString();
        }

        info = "(endDate : "+dat+" | reason : "+Integer.toString(this.getReason())+")";

        return info;
    }

    public String getEndDateStringFrench() {

        if (isInfiny()) {
            return "à vie";
        } else {
            SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
            String dat = formater.format(this.endDate);
            long diff = this.endDate.getTime() - UtilDate.getCurrentDate().getTime();
            dat = "jusqu'au "+dat + "\nreste "+ TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+" jours";

            return dat;
        }

    }
}

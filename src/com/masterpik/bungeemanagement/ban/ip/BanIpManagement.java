package com.masterpik.bungeemanagement.ban.ip;

import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.bungeemanagement.Main;
import com.masterpik.bungeemanagement.ban.reason.ReasonManagement;
import com.masterpik.database.api.Query;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class BanIpManagement {

    private static HashMap<String, BanIp> list;

    public static String kick;
    public static String prefix;
    public static String help;


    public static void BanIpInit() {
        list = new HashMap<>();
        list.putAll(getIps());

        kick = Api.getString("general.masterpik.main.logoText")+"\n \n"+Api.getString("proxy.ban.ip.kick");

        prefix = "banIp";
        help = prefix+" list of commands : \n /banip add [ip] [days|date|inf] [reason]\n /banip remove [ip]\n /banip modif [ip] [days|date|inf] [reason]\n /banip list\n /banip isBanIp [ip]\n /banip help [command]";


    }

    public static ArrayList<String> getList() {
        return new ArrayList<>(list.keySet());
    }

    public static BanIp getBanInfo(String ip) {
        if (BanIpManagement.isBan(ip)) {
            return list.get(ip);
        } else {
            return null;
        }
    }

    public static boolean addBan(BanIp ban) {
        if (!list.containsKey(ban.getIp())) {

            list.put(ban.getIp(), ban);

            int bucle = 0;

            while (bucle < ProxyServer.getInstance().getPlayers().size()) {

                ProxiedPlayer player = (ProxiedPlayer) ProxyServer.getInstance().getPlayers().toArray()[bucle];

                String ip = player.getPendingConnection().getAddress().toString().split(":")[0];
                ip = ip.replaceAll("/", "");

                if (ip.equalsIgnoreCase(ban.getIp())) {
                    String kik = BanIpManagement.kick;
                    kik = kik.replaceAll("%t", ban.getEndDateStringFrench());
                    kik = kik.replaceAll("%r", ReasonManagement.getReason(ban.getReason()));
                    player.disconnect(new TextComponent(kik));
                    bucle = ProxyServer.getInstance().getPlayers().size();
                }

                bucle++;
            }

            int ipCompress = UtilNetwork.ip2longCompressed(ban.getIp());
            String query1 = "INSERT INTO masterpik.ban_ip (ip, infinity_ban, ban_end, reason) VALUES (" + ipCompress + ", " + Boolean.toString(ban.isInfiny()).toUpperCase() + ", '" + ban.getEndDate().toString() + "', " + ban.getReason() + ")";
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);
            //return true;
        } else {
            return false;
        }
    }

    public static boolean removeBan(BanIp ban) {
        return removeBan(ban.getIp());
    }

    public static boolean removeBan(String ip) {
        if (list.containsKey(ip)) {
            list.remove(ip);

            int ipCompress = UtilNetwork.ip2longCompressed(ip);
            String query1 = "DELETE FROM masterpik.ban_ip WHERE ip = " + ipCompress;
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);

            //return true;
        } else {
            return false;
        }
    }

    public static boolean modifiyBan(String ip, BanIp newBan) {
        return removeBan(ip)
                && addBan(newBan);
    }

    public static boolean isBan(String ip) {
        boolean bool = false;

        if (list.containsKey(ip)) {

            if (!list.get(ip).isInfiny()
                    && list.get(ip).getEndDate().compareTo(UtilDate.getCurrentDate()) <= 0) {
                bool = false;
                removeBan(ip);
            } else if (list.get(ip).isInfiny()){
                bool = true;
            } else {
                bool = true;
            }

        } else {
            bool = false;
        }

        return bool;
    }

    private static HashMap<String, BanIp> getIps() {
        HashMap<String, BanIp> ips = new HashMap<>();

        String query1 = "SELECT * FROM masterpik.ban_ip";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            while(result1.next()) {
                String ip = UtilNetwork.longCompressed2Ip(result1.getInt("ip"));
                Boolean isInfiny = result1.getBoolean("infinity_ban");
                Date endDate = result1.getDate("ban_end");
                int reason = result1.getInt("reason");

                ips.put(ip, new BanIp(ip, isInfiny, endDate, reason));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ips;
    }

}

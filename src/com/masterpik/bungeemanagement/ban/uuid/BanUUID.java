package com.masterpik.bungeemanagement.ban.uuid;

import com.masterpik.api.util.UtilDate;
import com.masterpik.bungeeconnect.database.UUIDName;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BanUUID {

    private UUID uuid;
    private boolean isInfiny;

    private Date endDate;

    private int reason;

    public BanUUID(UUID uuid, boolean isInfiny, Date endDate, int reason) {
        this.uuid = uuid;
        this.isInfiny = isInfiny;
        this.endDate = endDate;
        this.reason = reason;
    }

    public UUID getUUID() {
        return uuid;
    }
    public void setUUID(UUID uuid) {
        this.uuid = uuid;
    }

    public boolean isInfiny() {
        return isInfiny;
    }
    public void setIsInfiny(boolean isInfiny) {
        this.isInfiny = isInfiny;
    }

    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getReason() {
        return reason;
    }
    public void setReason(int reason) {
        this.reason = reason;
    }

    public String toString() {
        String info = "";

        String dat = "";
        if (this.isInfiny()) {
            dat = " INFINITY ";
        } else {
            dat = " "+this.endDate.toString();
        }

        info = "(uuid : "+this.uuid+" ("+ UUIDName.uuidToPlayerName(this.uuid)+") | endDate : "+dat+" | reason : "+Integer.toString(this.getReason())+")";

        return info;
    }

    public String toStringNoUUID() {
        String info = "";

        String dat = "";
        if (this.isInfiny()) {
            dat = " INFINITY ";
        } else {
            dat = " "+this.endDate.toString();
        }

        info = "(endDate : "+dat+" | reason : "+Integer.toString(this.getReason())+")";

        return info;
    }

    public String getEndDateStringFrench() {

        if (isInfiny()) {
            return "à vie";
        } else {
            SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
            String dat = formater.format(this.endDate);
            long diff = this.endDate.getTime() - UtilDate.getCurrentDate().getTime();
            dat = "jusqu'au "+dat + "\nreste "+ TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)+" jours";

            return dat;
        }

    }

}

package com.masterpik.bungeemanagement.ban.uuid;

import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.bungeemanagement.Main;
import com.masterpik.bungeemanagement.ban.reason.ReasonManagement;
import com.masterpik.database.api.Query;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class BanUUIDManagement {

    private static HashMap<UUID, BanUUID> list;

    public static String kick;
    public static String prefix;
    public static String help;


    public static void BanUUIDInit() {
        list = new HashMap<>();
        list.putAll(getUUIDs());

        kick = Api.getString("general.masterpik.main.logoText")+"\n \n"+Api.getString("proxy.ban.uuid.kick");

        prefix = "banUUID";
        help = prefix+" list of commands : \n /banUUID add [uuid|player] [days|date|inf] [reason]\n /banUUID remove [uuid|player]\n /banUUID modif [uuid|player] [days|date|inf] [reason]\n /banUUID list\n /banUUID isBanUUID [uuid|player]\n /banUUID help [command]";


    }

    public static ArrayList<UUID> getList() {
        return new ArrayList<>(list.keySet());
    }

    public static BanUUID getBanInfo(UUID uuid) {
        if (isBan(uuid)) {
            return list.get(uuid);
        } else {
            return null;
        }
    }

    public static boolean addBan(BanUUID ban) {
        if (!list.containsKey(ban.getUUID())) {

            list.put(ban.getUUID(), ban);

            int bucle = 0;

            while (bucle < ProxyServer.getInstance().getPlayers().size()) {

                ProxiedPlayer player = (ProxiedPlayer) ProxyServer.getInstance().getPlayers().toArray()[bucle];

                UUID uuid = player.getUniqueId();

                if (uuid.compareTo(ban.getUUID()) == 0) {
                    String kik = BanUUIDManagement.kick;
                    kik = kik.replaceAll("%t", ban.getEndDateStringFrench());
                    kik = kik.replaceAll("%r", ReasonManagement.getReason(ban.getReason()));
                    player.disconnect(new TextComponent(kik));
                    bucle = ProxyServer.getInstance().getPlayers().size();
                }

                bucle++;
            }

            String query1 = "INSERT INTO masterpik.ban_uuid (uuid, infinity_ban, ban_end, reason) VALUES ('" + ban.getUUID().toString() + "', " + Boolean.toString(ban.isInfiny()).toUpperCase() + ", '" + ban.getEndDate().toString() + "', " + ban.getReason() + ")";
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);
            //return true;
        } else {
            return false;
        }
    }

    public static boolean removeBan(BanUUID ban) {
        return removeBan(ban.getUUID());
    }

    public static boolean removeBan(UUID uuid) {
        if (list.containsKey(uuid)) {
            list.remove(uuid);

            String query1 = "DELETE FROM masterpik.ban_uuid WHERE uuid = '" + uuid.toString()+"'";
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);

            //return true;
        } else {
            return false;
        }
    }

    public static boolean modifiyBan(UUID uuid, BanUUID newBan) {
        return removeBan(uuid)
                && addBan(newBan);
    }

    public static boolean isBan(UUID uuid) {
        boolean bool = false;

        if (list.containsKey(uuid)) {

            if (!list.get(uuid).isInfiny()
                    && list.get(uuid).getEndDate().compareTo(UtilDate.getCurrentDate()) <= 0) {
                bool = false;
                removeBan(uuid);
            } else if (list.get(uuid).isInfiny()){
                bool = true;
            } else {
                bool = true;
            }

        } else {
            bool = false;
        }

        return bool;
    }

    private static HashMap<UUID, BanUUID> getUUIDs() {
        HashMap<UUID, BanUUID> uuids = new HashMap<>();

        String query1 = "SELECT * FROM masterpik.ban_uuid";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            while(result1.next()) {
                UUID uuid = (UUID) result1.getObject("uuid");
                Boolean isInfiny = result1.getBoolean("infinity_ban");
                Date endDate = result1.getDate("ban_end");
                int reason = result1.getInt("reason");

                uuids.put(uuid, new BanUUID(uuid, isInfiny, endDate, reason));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return uuids;
    }

}

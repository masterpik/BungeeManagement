package com.masterpik.bungeemanagement.ban.uuid;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.database.UUIDName;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.sql.Date;
import java.util.ArrayList;
import java.util.UUID;

public class CommandsBanUUID extends Command {
    public CommandsBanUUID(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        BungeeLogger log = new BungeeLogger(sender, Colors.AQUA);

        String prefix = BanUUIDManagement.prefix;

        if (strings != null && strings.length >= 1) {
            if (strings.length >= 4
                    && (strings[0].equalsIgnoreCase("add")
                    || strings[0].equalsIgnoreCase("modif"))) {

                UUID uuid = null;

                if (UtilUUID.isAnUUID(strings[1])) {
                    uuid = UUID.fromString(strings[1]);
                } else if (UUIDName.playerNameToUUID(strings[1]) != null) {
                    uuid = UUIDName.playerNameToUUID(strings[1]);
                }


                boolean inf = false;
                Date date = UtilDate.getCurrentDate();

                if (strings[2].equalsIgnoreCase("inf")) {
                    inf = true;
                    date = Date.valueOf("3000-01-01");
                } else {
                    inf = false;
                    try {
                        date = Date.valueOf(strings[2]);
                    } catch(IllegalArgumentException e) {
                        try {

                            String type = "D";

                            if (strings[2].split("\\.").length >= 2) {

                                if (strings[0].equalsIgnoreCase("modif") && BanUUIDManagement.getBanInfo(uuid) != null) {
                                    date = BanUUIDManagement.getBanInfo(uuid).getEndDate();
                                } else {
                                    date = UtilDate.getCurrentDate();
                                }

                                int nb = Integer.parseInt(strings[2].split("\\.")[0]);
                                type = strings[2].split("\\.")[1];

                                if (UtilDate.getCalendarTypeFromString(type) != -1) {

                                    date = UtilDate.addToDate(date, UtilDate.getCalendarTypeFromString(type), nb);

                                } else {
                                    date = null;
                                }
                            }



                        } catch (NumberFormatException e2) {
                            date = null;
                        }
                    }
                }

                int reason = -1;

                try {
                    reason = Integer.parseInt(strings[3]);
                } catch (NumberFormatException e) {
                    reason = -1;
                }




                if (uuid != null
                        && date != null
                        && reason != -1) {

                    BanUUID ban = new BanUUID(uuid, inf, date, reason);

                    if (strings[0].equalsIgnoreCase("add")) {
                        if (BanUUIDManagement.addBan(ban)) {
                            log.info(prefix+" add ban for : "+strings[1]+" "+ban.toStringNoUUID());
                        } else {
                            log.info(prefix+" can't add ban for : "+strings[1]+" "+ban.toStringNoUUID());
                        }
                    } else if (strings[0].equalsIgnoreCase("modif")) {
                        if (BanUUIDManagement.modifiyBan(uuid, ban)) {
                            log.info(prefix+" modify ban for : "+strings[1]+" "+ban.toStringNoUUID());
                        } else {
                            log.info(prefix+" can't modify ban for : "+strings[1]+" "+ban.toStringNoUUID());
                        }
                    }

                } else {
                    log.info(prefix+" format error on one of this : (uuid|player : "+strings[1]+" | date : "+strings[2]+ " | reason : "+reason+")");
                }

            }
            else if (strings.length >=2
                    && (strings[0].equalsIgnoreCase("remove") || strings[0].equalsIgnoreCase("isBanUUID"))) {

                UUID uuid = null;

                if (UtilUUID.isAnUUID(strings[1])) {
                    uuid = UUID.fromString(strings[1]);
                } else if (UUIDName.playerNameToUUID(strings[1]) != null) {
                    uuid = UUIDName.playerNameToUUID(strings[1]);
                }

                if (uuid != null) {

                    if (strings[0].equalsIgnoreCase("remove")) {
                        if (BanUUIDManagement.removeBan(uuid)) {
                            log.info(prefix+" remove ban for : "+ strings[1]);
                        } else {
                            log.info(prefix+ " can't remove ban for : "+ strings[1]);
                        }
                    } else if (strings[0].equalsIgnoreCase("isBanUUID")) {
                        String info = "";

                        if (BanUUIDManagement.isBan(uuid)) {
                            BanUUID ban = BanUUIDManagement.getBanInfo(uuid);
                            info = " " +ban.toStringNoUUID();
                        }

                        log.info(prefix+" isBanUUID "+ strings[1]+" : "+Boolean.toString(BanUUIDManagement.isBan(uuid))+info);
                    }

                } else {
                    log.info(prefix+ " format error on uuid|player : "+strings[1]);
                }

            }
            else if (strings[0].equalsIgnoreCase("list")) {

                String str = prefix+" list : \n";
                //log.info("whitelistIp list : ");

                ArrayList<UUID> bans = new ArrayList<>();
                bans.addAll(BanUUIDManagement.getList());

                int bucle1 = 0;
                while (bucle1 < bans.size()) {

                    BanUUID ban= BanUUIDManagement.getBanInfo(bans.get(bucle1));

                    str = str + "  - "+ban.toString()+"\n";

                    bucle1++;
                }

                log.info(str);

            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                    if (strings[1].equalsIgnoreCase("add")) {
                        log.info(prefix+" help (add) : \n Description : Ban an uuid\n Usage : /banuuid add [uuid|player] [days|date|inf] [reason]");
                    } else if (strings[1].equalsIgnoreCase("remove")) {
                        log.info(prefix+" help (remove) : \n Description : Remove ban of an uuid\n Usage : /banuuid remove [uuid|player]");
                    } else if (strings[1].equalsIgnoreCase("modif")) {
                        log.info(prefix+" help (modif) : \n Description : Modify an existant ban\n Usage : /banuuid modif [uuid|player] [days|date|inf] [reason]");
                    } else if (strings[1].equalsIgnoreCase("isBanUUID")) {
                        log.info(prefix+" help (isBanUUID) : \n Description : Check if an uuid is ban \n Usage : /banuuid isBanUUID [uuid|player]");
                    } else if (strings[1].equalsIgnoreCase("help")) {
                        log.info(prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /banuuid help [command]");
                    } else {
                        log.info(BanUUIDManagement.help);
                    }

                } else {
                    log.info(BanUUIDManagement.help);
                }
            } else {
                log.info(BanUUIDManagement.help);
            }
        } else {
            log.info(BanUUIDManagement.help);
        }

    }

}

package com.masterpik.bungeemanagement.ban.reason;

import com.masterpik.api.util.UtilNetwork;
import com.masterpik.bungeemanagement.Main;
import com.masterpik.database.api.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class ReasonManagement {

    public static HashMap<Integer, String> reasons;

    public static void reasonInit() {
        reasons = new HashMap<>();
        reasons.putAll(getReasons());
    }

    public static String getReason(int reason) {
        if (reasons.containsKey(reason)) {
            return reasons.get(reason);
        } else {
            return Integer.toString(reason);
        }
    }

    public static HashMap<Integer, String> getReasons() {
        HashMap<Integer, String> rs = new HashMap<>();

        String query1 = "SELECT * FROM masterpik.ban_reason";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            while(result1.next()) {
                rs.put(result1.getInt("id"), result1.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rs;
    }

}

package com.masterpik.bungeemanagement.whitelist.ip;

import com.masterpik.api.util.UtilNetwork;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.database.IpPlayers;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeeconnect.objects.IP;
import com.masterpik.bungeemanagement.Main;
import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.database.api.Query;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class WhitelistIpManagement {

    private static ArrayList<String> list;
    public static String kick;
    public static String help;
    public static String prefix;

    public static void WhitelistIpInit() {
        list = new ArrayList<>();
        list.addAll(getIps());

        kick = Api.getString("general.masterpik.main.logoText")+"\n \n"+Api.getString("proxy.whitelist.ip.kick");

        prefix = "whitelistIp";

        help = prefix+" list of commands : \n /wip set [on|off]\n /wip add [ip]\n /wip remove [ip]\n /wip list\n /wip statu\n /wip isWip [ip]\n /wip help [command]";


    }

    public static ArrayList<String> getList() {
        return (ArrayList<String>) list.clone();
    }

    public static boolean remove(String arg) {
        if (UtilNetwork.checkIp(arg)) {
            return removeIp(arg);
        } else if (UtilUUID.isAnUUID(arg)) {
            return removeUUID(UUID.fromString(arg));
        } else if (UUIDName.playerNameToUUID(arg) != null) {
            return removeUUID(UUIDName.playerNameToUUID(arg));
        } else {
            return false;
        }
    }
    public static boolean removeUUID(UUID uuid) {
        if(IpPlayers.players.containsKey(uuid)) {

            ArrayList<String> ips = new ArrayList<>();
            ips.addAll(IpPlayers.players.get(uuid).getIps().keySet());

            int bucle = 0;

            while (bucle < ips.size()) {

                removeIp(ips.get(bucle));

                bucle++;
            }

            return true;

        } else {
            return false;
        }
    }
    public static boolean removeIp(String ip) {
        if (list.contains(ip)) {
            list.remove(ip);

            if (GeneralConfy.isWhitelistIp()) {
                int bucle = 0;

                while (bucle < ProxyServer.getInstance().getPlayers().size()) {

                    ProxiedPlayer player = (ProxiedPlayer) ProxyServer.getInstance().getPlayers().toArray()[bucle];

                    String stringIp = player.getAddress().toString().split(":")[0];
                    stringIp = stringIp.replaceAll("/", "");

                    if (stringIp.equals(ip)) {
                        player.disconnect(new TextComponent(kick));
                        bucle = ProxyServer.getInstance().getPlayers().size();
                    }

                    bucle++;
                }
            }

            int ipCompress = UtilNetwork.ip2longCompressed(ip);
            String query1 = "DELETE FROM masterpik.whitelist_ip WHERE ip = " + ipCompress;
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);
            //return true;
        } else {
            return false;
        }
    }

    public static boolean add(String arg) {
        if (UtilNetwork.checkIp(arg)) {
            return addIp(arg);
        } else if (UtilUUID.isAnUUID(arg)) {
            return addUUID(UUID.fromString(arg));
        } else if (UUIDName.playerNameToUUID(arg) != null) {
            return addUUID(UUIDName.playerNameToUUID(arg));
        } else {
            return false;
        }
    }
    public static boolean addUUID(UUID uuid) {
        if(IpPlayers.players.containsKey(uuid)) {

            ArrayList<String> ips = new ArrayList<>();
            ips.addAll(IpPlayers.players.get(uuid).getIps().keySet());

            int bucle = 0;

            while (bucle < ips.size()) {

                addIp(ips.get(bucle));

                bucle++;
            }

            return true;

        } else {
            return false;
        }
    }
    public static boolean addIp(String ip) {
        if (!list.contains(ip)) {
            list.add(ip);

            int ipCompress = UtilNetwork.ip2longCompressed(ip);
            String query1 = "INSERT INTO masterpik.whitelist_ip (ip) VALUES (" + ipCompress + ")";
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);
            //return true;
        } else {
            return false;
        }
    }

    public static boolean isWhitelist(String arg) {
        if (UtilNetwork.checkIp(arg)) {
            return isWhitelistIp(arg);
        } else if (UtilUUID.isAnUUID(arg)) {
            return isWhitelistUUID(UUID.fromString(arg));
        } else if (UUIDName.playerNameToUUID(arg) != null) {
            return isWhitelistUUID(UUIDName.playerNameToUUID(arg));
        } else {
            return false;
        }
    }
    public static boolean isWhitelistUUID(UUID uuid) {
        if(IpPlayers.players.containsKey(uuid)) {

            List<Boolean> bools = Collections.emptyList();

            ArrayList<String> ips = new ArrayList<>();
            ips.addAll(IpPlayers.players.get(uuid).getIps().keySet());

            int bucle = 0;

            while (bucle < ips.size()) {

                bools.add(isWhitelistIp(ips.get(bucle)));

                bucle++;
            }

            bools.removeIf(p -> !p);

            return bools.size() >= 1;

        } else {
            return false;
        }
    }
    public static boolean isWhitelistIp(String ip) {
        return list.contains(ip);
    }

    private static ArrayList<String> getIps() {
        ArrayList<String> ips = new ArrayList<>();

        String query1 = "SELECT * FROM masterpik.whitelist_ip";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            while(result1.next()) {
                String ip = UtilNetwork.longCompressed2Ip(result1.getInt("ip"));
                ips.add(ip);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ips;
    }

}

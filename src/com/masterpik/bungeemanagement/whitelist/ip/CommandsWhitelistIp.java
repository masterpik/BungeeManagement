package com.masterpik.bungeemanagement.whitelist.ip;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.bungeeconnect.database.IpPlayers;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeemanagement.confy.GeneralConfy;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

public class CommandsWhitelistIp extends Command {

    public CommandsWhitelistIp(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {


        BungeeLogger log = new BungeeLogger(sender, Colors.GREEN);
        //Logger log = ProxyServer.getInstance().getLogger();

        String prefix = WhitelistIpManagement.prefix;

        if (strings != null && strings.length >= 1) {

            if (strings.length >= 2
                    && strings[0].equalsIgnoreCase("set")
                    && (strings[1].equalsIgnoreCase("on") || strings[1].equalsIgnoreCase("off"))) {

                if (strings[1].equalsIgnoreCase("on")) {
                    GeneralConfy.setWhitelistIp(true);
                    log.info(""+prefix+" set to on");
                } else if (strings[1].equalsIgnoreCase("off")){
                    GeneralConfy.setWhitelistIp(false);
                    log.info(prefix+" set to off");
                }

            }
            else if (strings.length >=2
                    && (strings[0].equalsIgnoreCase("add") || strings[0].equalsIgnoreCase("remove") || strings[0].equalsIgnoreCase("isWip"))) {

                String arg = strings[1];

                if (strings[0].equalsIgnoreCase("add")) {
                    if (WhitelistIpManagement.add(arg)) {
                        log.info(prefix + " add " + arg);
                    } else {
                        log.info(""+prefix+" can't add "+ arg+" to the whitelist");
                    }
                }
                else if (strings[0].equalsIgnoreCase("remove")) {
                    if (WhitelistIpManagement.remove(arg)) {
                        log.info(prefix + " remove " + arg);
                    } else {
                        log.info(prefix+" can't remove "+arg+" of the whitelist");
                    }
                }
                else if (strings[0].equalsIgnoreCase("isWip")) {
                    log.info(prefix+" isWip "+arg+" : "+WhitelistIpManagement.isWhitelist(arg));
                }

            }
            else if (strings[0].equalsIgnoreCase("list")) {

                String str = prefix+" list : \n active : "+GeneralConfy.isWhitelistIp()+"\n";
                //log.info("whitelistIp list : ");

                ArrayList<String> ips = new ArrayList<>();
                ips.addAll(WhitelistIpManagement.getList());

                int bucle1 = 0;
                while (bucle1 < ips.size()) {

                    String players = "("+IpPlayers.getPlayersFromIp(ips.get(bucle1))+")";

                    str = str + "  - "+ips.get(bucle1)+" "+players+"\n";

                    bucle1++;
                }

                log.info(str);

            }
            else if (strings[0].equalsIgnoreCase("statu")) {
                log.info(prefix+" isActive : " + GeneralConfy.isWhitelistIp());
            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                   if (strings[1].equalsIgnoreCase("set")) {
                       log.info(prefix+" help (set) : \n Description : Set the "+prefix+" statu \n Usage : /wip set [on|off]");
                   } else if (strings[1].equalsIgnoreCase("add")) {
                       log.info(prefix+" help (add) : \n Description : Add an ip to the "+prefix+" \n Usage : /wip add [ip]");
                   } else if (strings[1].equalsIgnoreCase("remove")) {
                       log.info(prefix+" help (remove) : \n Description : Remove an ip to the "+prefix+" \n Usage : /wip remove [ip]");
                   } else if (strings[1].equalsIgnoreCase("list")) {
                       log.info(prefix+" help (list) : \n Description : Get the "+prefix+" list \n Usage : /wip list");
                   } else if (strings[1].equalsIgnoreCase("statu")) {
                       log.info(prefix+" help (statu) : \n Description : Get the "+prefix+" statu \n Usage : /wip statu");
                   } else if (strings[1].equalsIgnoreCase("isWip")) {
                       log.info(prefix+" help (isWip) : \n Description : Get if an ip is "+prefix+" \n Usage : /wip isWip");
                   } else if (strings[1].equalsIgnoreCase("help")) {
                       log.info(prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /wip help [command]");
                   } else {
                       log.info(WhitelistIpManagement.help);
                   }

                } else {
                    log.info(WhitelistIpManagement.help);
                }
            } else {
                log.info(WhitelistIpManagement.help);
            }

        } else {
            log.info(WhitelistIpManagement.help);
        }
    }
}

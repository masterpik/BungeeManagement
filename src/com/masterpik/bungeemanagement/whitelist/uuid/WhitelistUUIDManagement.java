package com.masterpik.bungeemanagement.whitelist.uuid;

import com.masterpik.api.util.UtilNetwork;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeemanagement.Main;
import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.database.api.Query;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

public class WhitelistUUIDManagement {

    private static ArrayList<UUID> list;
    public static String kick;
    public static String help;
    public static String prefix;

    public static void WhitelistUUIDInit() {
        list = new ArrayList<>();
        list.addAll(getUUIDs());

        kick = Api.getString("general.masterpik.main.logoText")+"\n \n"+Api.getString("proxy.whitelist.uuid.kick");

        prefix = "whitelistUUID";

        help = ""+prefix+" list of commands : \n /wuuid set [on|off]\n /wuuid add [uuid]\n /wuuid remove [uuid]\n /wuuid list\n /wuuid statu\n /wuuid isWuuid [uuid]\n /wuuid help [command]";
    }

    public static ArrayList<UUID> getList() {
        return (ArrayList<UUID>) list.clone();
    }

    public static boolean removeUUID(Object remove) {
        if (remove instanceof UUID) {
            return removeUUID((UUID) remove);
        } else if (remove instanceof String) {
            return removeUUID((String) remove);
        } else {
            return false;
        }
    }
    public static boolean removeUUID(String playerName) {
        UUID uuid = UUIDName.playerNameToUUID(playerName);
        if (uuid != null) {
          return removeUUID(uuid);
        } else {
            return false;
        }
    }
    public static boolean removeUUID(UUID uuid) {
        if (list.contains(uuid)) {
            list.remove(uuid);

            if (GeneralConfy.isWhitelistUUID()) {
                int bucle = 0;

                while (bucle < ProxyServer.getInstance().getPlayers().size()) {

                    ProxiedPlayer player = (ProxiedPlayer) ProxyServer.getInstance().getPlayers().toArray()[bucle];

                    UUID playerUUID = player.getUniqueId();

                    if (playerUUID.equals(uuid)) {
                        player.disconnect(new TextComponent(kick));
                        bucle = ProxyServer.getInstance().getPlayers().size();
                    }

                    bucle++;
                }
            }

            String query1 = "DELETE FROM masterpik.whitelist_uuid WHERE uuid = '" + uuid + "'";
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);
            //return true;
        } else {
            return false;
        }
    }

    public static boolean addUUID(Object add) {
        if (add instanceof UUID) {
            return addUUID((UUID) add);
        } else if (add instanceof String) {
            return addUUID((String) add);
        } else {
            return false;
        }
    }
    public static boolean addUUID(String playerName) {
        UUID uuid = UUIDName.playerNameToUUID(playerName);
        if (uuid != null) {
            return addUUID(uuid);
        } else {
            return false;
        }
    }
    public static boolean addUUID(UUID uuid) {
        if (!list.contains(uuid)) {
            list.add(uuid);

            String query1 = "INSERT INTO masterpik.whitelist_uuid (uuid) VALUES ('" + uuid + "')";
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);
            //return true;
        } else {
            return false;
        }
    }

    public static boolean isWhitelist(Object test) {
        if (test instanceof UUID) {
            return isWhitelist((UUID) test);
        } else if (test instanceof String) {
            return isWhitelist((String) test);
        } else {
            return false;
        }
    }
    public static boolean isWhitelist(String playerName) {
        UUID uuid = UUIDName.playerNameToUUID(playerName);
        if (uuid != null) {
            return isWhitelist(uuid);
        } else {
            return false;
        }
    }
    public static boolean isWhitelist(UUID uuid) {
        return list.contains(uuid);
    }

    private static ArrayList<UUID> getUUIDs() {
        ArrayList<UUID> uuids = new ArrayList<>();

        String query1 = "SELECT * FROM masterpik.whitelist_uuid";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            while(result1.next()) {
                uuids.add((UUID) result1.getObject("uuid"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return uuids;
    }

}

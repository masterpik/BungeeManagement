package com.masterpik.bungeemanagement.whitelist.uuid;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.database.IpPlayers;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.bungeemanagement.whitelist.ip.WhitelistIpManagement;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

public class CommandsWhitelistUUID extends Command {
    public CommandsWhitelistUUID(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        /*if (sender instanceof ProxiedPlayer) {

        } else {
        }*/
        //Logger log = ProxyServer.getInstance().getLogger();
        BungeeLogger log = new BungeeLogger(sender, Colors.GREEN);

        String prefix = WhitelistUUIDManagement.prefix;

        if (strings != null && strings.length >= 1) {


            if (strings.length >= 2
                    && strings[0].equalsIgnoreCase("set")
                    && (strings[1].equalsIgnoreCase("on") || strings[1].equalsIgnoreCase("off"))) {

                if (strings[1].equalsIgnoreCase("on")) {
                    GeneralConfy.setWhitelistUUID(true);
                    log.info(""+prefix+" set to on");
                } else if (strings[1].equalsIgnoreCase("off")){
                    GeneralConfy.setWhitelistUUID(false);
                    log.info(""+prefix+" set to off");
                }

            }
            else if (strings.length >=2
                    && (strings[0].equalsIgnoreCase("add") || strings[0].equalsIgnoreCase("remove") || strings[0].equalsIgnoreCase("isWuuid"))) {

                Object arg = null;

                if (UtilUUID.isAnUUID(strings[1])) {
                    arg = UUID.fromString(strings[1]);
                } else {
                    arg = strings[1];
                }

                if (strings[0].equalsIgnoreCase("add")) {
                    if (WhitelistUUIDManagement.addUUID(arg)) {
                        log.info(""+prefix+" add " + arg);
                    } else {
                        log.info(""+prefix+" can't add "+ arg+" to the whitelist");
                    }
                }
                else if (strings[0].equalsIgnoreCase("remove")) {
                    if (WhitelistUUIDManagement.removeUUID(arg)) {
                        log.info(""+prefix+" remove " + arg);
                    } else {
                        log.info(prefix+" can't remove "+arg+" of the whitelist");
                    }
                }
                else if (strings[0].equalsIgnoreCase("isWuuid")) {
                    log.info(""+prefix+" isWuuid "+arg+" : "+WhitelistUUIDManagement.isWhitelist(arg));
                }

            }
            else if (strings[0].equalsIgnoreCase("list")) {
                String str = ""+prefix+" list : \n active : "+GeneralConfy.isWhitelistUUID()+"\n";
                //log.info("whitelistIp list : ");

                ArrayList<UUID> uuids = new ArrayList<>();
                uuids.addAll(WhitelistUUIDManagement.getList());

                int bucle1 = 0;
                while (bucle1 < uuids.size()) {


                    //log.info("  - "+ips.get(bucle1)+" "+players);

                    str = str + "  - "+uuids.get(bucle1)+" ("+UUIDName.uuidToPlayerName(uuids.get(bucle1))+")\n";

                    bucle1++;
                }

                log.info(str);

            }
            else if (strings[0].equalsIgnoreCase("statu")) {
                log.info(""+prefix+" isActive : "+GeneralConfy.isWhitelistUUID());
            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                    if (strings[1].equalsIgnoreCase("set")) {
                        log.info(""+prefix+" help (set) : \n Description : Set the "+prefix+" statu \n Usage : /wuuid set [on|off]");
                    } else if (strings[1].equalsIgnoreCase("add")) {
                        log.info(""+prefix+" help (add) : \n Description : Add a uuid to the "+prefix+" \n Usage : /wuuid add [uuid]");
                    } else if (strings[1].equalsIgnoreCase("remove")) {
                        log.info(""+prefix+" help (remove) : \n Description : Remove a uuid to the "+prefix+" \n Usage : /wuuid remove [uuid]");
                    } else if (strings[1].equalsIgnoreCase("list")) {
                        log.info(""+prefix+" help (list) : \n Description : Get the "+prefix+" list \n Usage : /wuuid list");
                    } else if (strings[1].equalsIgnoreCase("statu")) {
                        log.info(""+prefix+" help (statu) : \n Description : Get the "+prefix+" statu \n Usage : /wuuid statu");
                    } else if (strings[1].equalsIgnoreCase("isWuuid")) {
                        log.info(""+prefix+" help (isWuuid) : \n Description : Get if a uuid is whitelist \n Usage : /wuuid isWuuid");
                    } else if (strings[1].equalsIgnoreCase("help")) {
                        log.info(""+prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /wuuid help [command]");
                    } else {
                        log.info(WhitelistUUIDManagement.help);
                    }

                } else {
                    log.info(WhitelistUUIDManagement.help);
                }
            } else {
                log.info(WhitelistUUIDManagement.help);
            }

        } else {
            log.info(WhitelistUUIDManagement.help);
        }

    }
}

package com.masterpik.bungeemanagement.maintenance;

import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.messages.bungee.Api;

public class MaintenanceManagement {

    public static String kick;
    public static String prefix;
    public static String help;

    public static void MaintenanceInit() {

        MaintenanceManagement.getKickMessage();

        prefix = "maintenance";

        help = ""+prefix+" list of commands : \n /maintenance set [on|off] [type]\n /maintenance setType [type]\n /maintenance statu\n /maintenance help [command]";

    }

    public static void getKickMessage() {
        kick = Api.getString("general.masterpik.main.logoText")+"\n \n"+Api.getString("proxy.maintenance.kick."+Integer.toString(GeneralConfy.getMaintenanceKick()));
    }

}

package com.masterpik.bungeemanagement.maintenance;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeemanagement.confy.GeneralConfy;
import com.masterpik.bungeemanagement.whitelist.uuid.WhitelistUUIDManagement;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.UUID;

public class CommandsMaintenance extends Command {
    public CommandsMaintenance(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        BungeeLogger log = new BungeeLogger(sender, Colors.RED);

        String prefix = MaintenanceManagement.prefix;

        if (strings != null && strings.length >= 1) {

            if (strings.length >= 2
                    && ((strings[0].equalsIgnoreCase("set") && (strings[1].equalsIgnoreCase("on") || strings[1].equalsIgnoreCase("off")))
                    || (strings[0].equalsIgnoreCase("setType")))) {

                if (strings[0].equalsIgnoreCase("set")) {

                    if (strings[1].equalsIgnoreCase("on")) {

                        int type = GeneralConfy.getMaintenanceKick();

                        if (strings.length >= 3) {
                            type = Integer.parseInt(strings[2]);
                        }

                        GeneralConfy.setMaintenance(true, type);
                        log.info("" + prefix + " set to on with type "+Integer.toString(type));
                    } else if (strings[1].equalsIgnoreCase("off")) {
                        GeneralConfy.setMaintenance(false, GeneralConfy.getMaintenanceKick());
                        log.info("" + prefix + " set to off");
                    }
                } else if (strings[0].equalsIgnoreCase("setType")) {
                    GeneralConfy.setMaintenanceKick(Integer.parseInt(strings[1]));
                    log.info(prefix + " change type to "+Integer.parseInt(strings[1]));
                }

            }
            else if (strings[0].equalsIgnoreCase("statu")) {

                String plus = "";
                if (GeneralConfy.isMainteance()) {
                    plus = " with type "+GeneralConfy.getMaintenanceKick();
                }

                log.info(""+prefix+" isActive : "+GeneralConfy.isMainteance()+plus);
            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                    if (strings[1].equalsIgnoreCase("set")) {
                        log.info(""+prefix+" help (set) : \n Description : Set the "+prefix+" statu and type \n Usage : /maintenance set [on|off] [type]");
                    } else if (strings[1].equalsIgnoreCase("setType")) {
                        log.info(""+prefix+" help (setType) : \n Description : Set the type of the "+prefix+" \n Usage : /maintenance setType [type]");
                    }else if (strings[1].equalsIgnoreCase("statu")) {
                        log.info(""+prefix+" help (statu) : \n Description : Get the "+prefix+" statu \n Usage : /maintenance statu");
                    } else if (strings[1].equalsIgnoreCase("help")) {
                        log.info(""+prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /maintenance help [command]");
                    } else {
                        log.info(MaintenanceManagement.help);
                    }

                } else {
                    log.info(MaintenanceManagement.help);
                }
            } else {
                log.info(MaintenanceManagement.help);
            }

        }
        else {
            log.info(MaintenanceManagement.help);
        }


    }
}

package com.masterpik.bungeemanagement.mute;

import com.masterpik.api.util.UtilDate;
import com.masterpik.bungeeconnect.database.UUIDName;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Mute {
    private UUID uuid;

    private Timestamp endDate;

    private int reason;

    public Mute(UUID uuid, Timestamp endDate, int reason) {
        this.uuid = uuid;
        this.endDate = endDate;
        this.reason = reason;
    }

    public UUID getUUID() {
        return uuid;
    }
    public void setUUID(UUID uuid) {
        this.uuid = uuid;
    }

    public Timestamp getEndDate() {
        return endDate;
    }
    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public int getReason() {
        return reason;
    }
    public void setReason(int reason) {
        this.reason = reason;
    }

    public String toString() {
        String info = "";

        String dat = this.endDate.toString();

        info = "(uuid : "+this.uuid+" ("+ UUIDName.uuidToPlayerName(this.uuid)+") | endDate : "+dat+" | reason : "+Integer.toString(this.getReason())+")";

        return info;
    }

    public String toStringNoUUID() {
        String info = "";

        String dat = this.endDate.toString();

        info = "(endDate : "+dat+" | reason : "+Integer.toString(this.getReason())+")";

        return info;
    }

    public String getEndDateStringFrench() {

        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        String dat = formater.format(this.endDate);
        long diff = this.endDate.getTime() - UtilDate.getCurrentTimeStamp().getTime();
        dat = "jusqu'au "+dat + "\nreste "+ TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS)+" minute(s)";

        return dat;

    }
}

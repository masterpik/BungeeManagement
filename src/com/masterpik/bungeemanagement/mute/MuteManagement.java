package com.masterpik.bungeemanagement.mute;

import com.masterpik.api.util.UtilDate;
import com.masterpik.bungeemanagement.Main;
import com.masterpik.bungeemanagement.mute.reason.ReasonManagement;
import com.masterpik.database.api.Query;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class MuteManagement {
    private static HashMap<UUID, Mute> list;

    public static String kick;
    public static String prefix;
    public static String help;


    public static void MuteInit() {
        list = new HashMap<>();
        list.putAll(getUUIDs());

        kick = Api.getString("noCheat.chat.mainPrefix")+" "+Api.getString("proxy.mute.kick");

        prefix = "mute";
        help = prefix+" list of commands : \n /mute add [uuid|player] [days|date] [reason]\n /mute remove [uuid|player]\n /mute modif [uuid|player] [days|date] [reason]\n /mute list\n /mute isMute [uuid|player]\n /mute help [command]";


    }

    public static ArrayList<UUID> getList() {
        return new ArrayList<>(list.keySet());
    }

    public static Mute getMuteInfo(UUID uuid) {
        if (isMute(uuid)) {
            return list.get(uuid);
        } else {
            return null;
        }
    }

    public static boolean addMute(Mute mute) {
        if (!list.containsKey(mute.getUUID())) {

            list.put(mute.getUUID(), mute);

            int bucle = 0;

            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(mute.getUUID());
            if (player != null
                    && player.isConnected()) {
                player.sendMessage(new TextComponent(getMuteMsg(mute)));
            }

            String query1 = "INSERT INTO masterpik.mute (uuid, mute_end, reason) VALUES ('" + mute.getUUID().toString() + "', '" + mute.getEndDate().toString() + "', " + mute.getReason() + ")";
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);
            //return true;
        } else {
            return false;
        }
    }

    public static boolean removeMute(Mute mute) {
        return removeMute(mute.getUUID());
    }

    public static boolean removeMute(UUID uuid) {
        if (list.containsKey(uuid)) {
            list.remove(uuid);

            String query1 = "DELETE FROM masterpik.mute WHERE uuid = '" + uuid.toString()+"'";
            //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
            return Query.queryInput(Main.dbConnection, query1);

            //return true;
        } else {
            return false;
        }
    }

    public static boolean modifiyMute(UUID uuid, Mute newMute) {
        return removeMute(uuid)
                && addMute(newMute);
    }

    public static boolean isMute(UUID uuid) {
        boolean bool = false;

        if (list.containsKey(uuid)) {

            if (list.get(uuid).getEndDate().compareTo(UtilDate.getCurrentDate()) <= 0) {
                bool = false;
                removeMute(uuid);
            } else {
                bool = true;
            }

        } else {
            bool = false;
        }

        return bool;
    }

    private static HashMap<UUID, Mute> getUUIDs() {
        HashMap<UUID, Mute> uuids = new HashMap<>();

        String query1 = "SELECT * FROM masterpik.mute";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {
            while(result1.next()) {
                UUID uuid = (UUID) result1.getObject("uuid");
                //TODO timeStamp
                //Timestamp endDate = result1.getDate("ban_end");
                int reason = result1.getInt("reason");

                //uuids.put(uuid, new Mute(uuid, endDate, reason));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return uuids;
    }

    public static String getMuteMsg(Mute mute) {
        String mut = kick;
        mut = mut.replaceAll("%t", mute.getEndDateStringFrench());
        mut = mut.replaceAll("%r", ReasonManagement.getReason(mute.getReason()));
        return mut;
    }
}

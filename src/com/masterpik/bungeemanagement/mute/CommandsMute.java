package com.masterpik.bungeemanagement.mute;

import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.database.UUIDName;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

public class CommandsMute extends Command {
    public CommandsMute(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        BungeeLogger log = new BungeeLogger(sender, Colors.AQUA);

        String prefix = MuteManagement.prefix;

        if (strings != null && strings.length >= 1) {
            if (strings.length >= 4
                    && (strings[0].equalsIgnoreCase("add")
                    || strings[0].equalsIgnoreCase("modif"))) {

                UUID uuid = null;

                if (UtilUUID.isAnUUID(strings[1])) {
                    uuid = UUID.fromString(strings[1]);
                } else if (UUIDName.playerNameToUUID(strings[1]) != null) {
                    uuid = UUIDName.playerNameToUUID(strings[1]);
                }


                Timestamp date = UtilDate.getCurrentTimeStamp();

                try {
                    date = Timestamp.valueOf(strings[2]);
                } catch(IllegalArgumentException e) {
                    try {

                        String type = "D";

                        if (strings[2].split("\\.").length >= 2) {

                            if (strings[0].equalsIgnoreCase("modif") && MuteManagement.getMuteInfo(uuid) != null) {
                                date = MuteManagement.getMuteInfo(uuid).getEndDate();
                            } else {
                                date = UtilDate.getCurrentTimeStamp();
                            }

                            int nb = Integer.parseInt(strings[2].split("\\.")[0]);
                            type = strings[2].split("\\.")[1];

                            if (UtilDate.getCalendarTypeFromString(type) != -1) {

                                date = UtilDate.addToTimeStamp(date, UtilDate.getCalendarTypeFromString(type), nb);

                            } else {
                                date = null;
                            }
                        }



                    } catch (NumberFormatException e2) {
                        date = null;
                    }
                }

                int reason = -1;

                try {
                    reason = Integer.parseInt(strings[3]);
                } catch (NumberFormatException e) {
                    reason = -1;
                }




                if (uuid != null
                        && date != null
                        && reason != -1) {

                    Mute mute = new Mute(uuid, date, reason);

                    if (strings[0].equalsIgnoreCase("add")) {
                        if (MuteManagement.addMute(mute)) {
                            log.info(prefix+" add mute for : "+strings[1]+" "+mute.toStringNoUUID());
                        } else {
                            log.info(prefix+" can't add mute for : "+strings[1]+" "+mute.toStringNoUUID());
                        }
                    } else if (strings[0].equalsIgnoreCase("modif")) {
                        if (MuteManagement.modifiyMute(uuid, mute)) {
                            log.info(prefix+" modify mute for : "+strings[1]+" "+mute.toStringNoUUID());
                        } else {
                            log.info(prefix+" can't modify mute for : "+strings[1]+" "+mute.toStringNoUUID());
                        }
                    }

                } else {
                    log.info(prefix+" format error on one of this : (uuid|player : "+strings[1]+" | date : "+strings[2]+ " | reason : "+reason+")");
                }

            }
            else if (strings.length >=2
                    && (strings[0].equalsIgnoreCase("remove") || strings[0].equalsIgnoreCase("isBanUUID"))) {

                UUID uuid = null;

                if (UtilUUID.isAnUUID(strings[1])) {
                    uuid = UUID.fromString(strings[1]);
                } else if (UUIDName.playerNameToUUID(strings[1]) != null) {
                    uuid = UUIDName.playerNameToUUID(strings[1]);
                }

                if (uuid != null) {

                    if (strings[0].equalsIgnoreCase("remove")) {
                        if (MuteManagement.removeMute(uuid)) {
                            log.info(prefix+" remove mute for : "+ strings[1]);
                        } else {
                            log.info(prefix+ " can't remove mute for : "+ strings[1]);
                        }
                    } else if (strings[0].equalsIgnoreCase("isMute")) {
                        String info = "";

                        if (MuteManagement.isMute(uuid)) {
                            Mute mute = MuteManagement.getMuteInfo(uuid);
                            info = " " +mute.toStringNoUUID();
                        }

                        log.info(prefix+" isMute "+ strings[1]+" : "+Boolean.toString(MuteManagement.isMute(uuid))+info);
                    }

                } else {
                    log.info(prefix+ " format error on uuid|player : "+strings[1]);
                }

            }
            else if (strings[0].equalsIgnoreCase("list")) {

                String str = prefix+" list : \n";
                //log.info("whitelistIp list : ");

                ArrayList<UUID> mutes = new ArrayList<>();
                mutes.addAll(MuteManagement.getList());

                int bucle1 = 0;
                while (bucle1 < mutes.size()) {

                    Mute mute = MuteManagement.getMuteInfo(mutes.get(bucle1));

                    str = str + "  - "+mute.toString()+"\n";

                    bucle1++;
                }

                log.info(str);

            }
            else if (strings[0].equalsIgnoreCase("help")) {
                if (strings.length >= 2) {

                    if (strings[1].equalsIgnoreCase("add")) {
                        log.info(prefix+" help (add) : \n Description : Mute an uuid\n Usage : /mute add [uuid|player] [days|date] [reason]");
                    } else if (strings[1].equalsIgnoreCase("remove")) {
                        log.info(prefix+" help (remove) : \n Description : Remove mute of an uuid\n Usage : /mute remove [uuid|player]");
                    } else if (strings[1].equalsIgnoreCase("modif")) {
                        log.info(prefix+" help (modif) : \n Description : Modify an existant mute\n Usage : /mute modif [uuid|player] [days|date] [reason]");
                    } else if (strings[1].equalsIgnoreCase("isMute")) {
                        log.info(prefix+" help (isMute) : \n Description : Check if an uuid is mute \n Usage : /mute isMute [uuid|player]");
                    } else if (strings[1].equalsIgnoreCase("help")) {
                        log.info(prefix+" help (help) : \n Description : Get the help for a "+prefix+" command \n Usage : /mute help [command]");
                    } else {
                        log.info(MuteManagement.help);
                    }

                } else {
                    log.info(MuteManagement.help);
                }
            } else {
                log.info(MuteManagement.help);
            }
        } else {
            log.info(MuteManagement.help);
        }
    }
}

package com.masterpik.bungeemanagement.confy;

import com.masterpik.bungeemanagement.Main;
import com.masterpik.bungeemanagement.commands.CommandsPerms;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class CommandsPermsConfy {
    public static Configuration commandPermsConfy;
    public static File commandPermsFile;


    public static void CommandsPermsInit() {
        try {
            if (!Main.plugin.getDataFolder().exists()) {
                Main.plugin.getDataFolder().mkdir();
            }

            commandPermsFile = new File(Main.plugin.getDataFolder().getPath(), "commandsPerms.yml");

            if (!commandPermsFile.exists()) {
                commandPermsFile.createNewFile();
                commandPermsConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(commandPermsFile);



                HashMap<String, ArrayList<String>> perms = new HashMap<>();

                ArrayList<String> allCmd = new ArrayList<>();
                allCmd.add("/hub");
                allCmd.add("/lobby");
                perms.put("*", allCmd);

                ArrayList<String> opCmd = new ArrayList<>();
                opCmd.add("*");
                perms.put("operator", opCmd);

                ArrayList<String> testCmd = new ArrayList<>();
                testCmd.add("/spawn");
                perms.put("test", testCmd);


                ArrayList<String> players = new ArrayList<>();
                players.addAll(perms.keySet());

                commandPermsConfy.set("CommandsPerms.list", players);

                int bucle = 0;
                while (bucle < players.size()) {
                    commandPermsConfy.set("CommandsPerms."+players.get(bucle), perms.get(players.get(bucle)));
                    bucle++;
                }

                CommandsPermsConfy.saveFile(commandPermsConfy, commandPermsFile);
            }
            else {
                commandPermsConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(commandPermsFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    public static ArrayList<String> getList() {
        ArrayList<String> list = new ArrayList<>();

        list.addAll(commandPermsConfy.getStringList("CommandsPerms.list"));

        return list;
    }

    public static HashMap<String, ArrayList<String>> getHashMap() {
        HashMap<String, ArrayList<String>> map = new HashMap<>();

        ArrayList<String> list = getList();

        int bucle = 0;

        while (bucle < list.size()) {

            map.put(list.get(bucle), new ArrayList<String>(commandPermsConfy.getStringList("CommandsPerms."+list.get(bucle))));

            bucle++;
        }

        return map;
    }


    public static void removeCommand(String uuid, String command) {

        ArrayList<String> commands = new ArrayList<>();
        commands.addAll(CommandsPerms.permissions.get(uuid));

        commands.remove(command);

        if (commands.size() > 0) {
            commandPermsConfy.set("CommandsPerms."+uuid, commands);
            CommandsPerms.permissions.get(uuid).remove(command);
        } else {
            commandPermsConfy.set("CommandsPerms."+uuid, null);
            CommandsPerms.permissions.remove(uuid);

            ArrayList<String> allList = new ArrayList<>();
            allList.addAll(CommandsPerms.permissions.keySet());
            allList.remove(uuid);
            commandPermsConfy.set("CommandsPerms.list", allList);
        }

        CommandsPermsConfy.saveFile(commandPermsConfy, commandPermsFile);
    }

    public static void addCommand(String uuid, String command) {

        ArrayList<String> commands = new ArrayList<>();

        if (CommandsPerms.permissions.containsKey(uuid)) {
            commands.addAll(CommandsPerms.permissions.get(uuid));

        } else {
            ArrayList<String> allList = new ArrayList<>();
            allList.addAll(CommandsPerms.permissions.keySet());
            allList.add(uuid);
            commandPermsConfy.set("CommandsPerms.list", allList);
        }

        commands.add(command);

        commandPermsConfy.set("CommandsPerms."+uuid, commands);


        if (CommandsPerms.permissions.containsKey(uuid)) {
            CommandsPerms.permissions.get(uuid).add(command);
        } else {
            CommandsPerms.permissions.put(uuid, commands);
        }

        CommandsPermsConfy.saveFile(commandPermsConfy, commandPermsFile);
    }


    public static void saveFile(Configuration file, File fileg) {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(file, fileg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void reload() {
        try {
            commandPermsConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(commandPermsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

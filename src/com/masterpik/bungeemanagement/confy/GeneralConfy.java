package com.masterpik.bungeemanagement.confy;

import com.masterpik.bungeemanagement.Main;
import com.masterpik.bungeemanagement.maintenance.MaintenanceManagement;
import com.masterpik.bungeemanagement.whitelist.ip.WhitelistIpManagement;
import com.masterpik.bungeemanagement.whitelist.uuid.WhitelistUUIDManagement;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class GeneralConfy {

    public static Configuration generalConfy;
    public static File generalFile;


    public static void GeneralInit() {
        try {
            if (!Main.plugin.getDataFolder().exists()) {
                Main.plugin.getDataFolder().mkdir();
            }

            generalFile = new File(Main.plugin.getDataFolder().getPath(), "general.yml");

            if (!generalFile.exists()) {
                generalFile.createNewFile();
                generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);

                generalConfy.set("General.whitelist.ip.active", false);
                generalConfy.set("General.whitelist.uuid.active", false);

                generalConfy.set("General.maintenance.active", false);
                generalConfy.set("General.maintenance.kick", 1);

                generalConfy.set("General.ping.max", 500);

                GeneralConfy.saveFile(generalConfy, generalFile);
            }
            else {
                generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static boolean isWhitelistIp() {
        return generalConfy.getBoolean("General.whitelist.ip.active");
    }
    public static void setWhitelistIp(boolean statu) {
        generalConfy.set("General.whitelist.ip.active", statu);
        GeneralConfy.saveFile(generalConfy, generalFile);
    }

    public static boolean isWhitelistUUID() {
        return generalConfy.getBoolean("General.whitelist.uuid.active");
    }
    public static void setWhitelistUUID(boolean statu) {
        generalConfy.set("General.whitelist.uuid.active", statu);
        GeneralConfy.saveFile(generalConfy, generalFile);

        if (statu) {
            ArrayList<ProxiedPlayer> players = new ArrayList<>();
            players.addAll(ProxyServer.getInstance().getPlayers());

            int bucle = 0;

            while (bucle < players.size()) {

                if (!WhitelistUUIDManagement.isWhitelist(players.get(bucle).getUniqueId())) {
                    players.get(bucle).disconnect(new TextComponent(WhitelistUUIDManagement.kick));
                }

                bucle ++;
            }
        }

    }

    public static boolean isMainteance() {
        return generalConfy.getBoolean("General.maintenance.active");
    }
    public static int getMaintenanceKick() {
        return generalConfy.getInt("General.maintenance.kick");
    }
    public static void setMaintenanceKick(int type) {
        generalConfy.set("General.maintenance.kick", type);
        GeneralConfy.saveFile(generalConfy, generalFile);
        MaintenanceManagement.getKickMessage();
    }
    public static void setMaintenance(boolean statu, int type) {

        generalConfy.set("General.maintenance.active", statu);
        if (statu) {
            generalConfy.set("General.maintenance.kick", type);
        }
        GeneralConfy.saveFile(generalConfy, generalFile);


        if (statu) {

            MaintenanceManagement.getKickMessage();

            ArrayList<ProxiedPlayer> players = new ArrayList<>();
            players.addAll(ProxyServer.getInstance().getPlayers());

            int bucle = 0;

            while (bucle < players.size()) {

                String ip = players.get(bucle).getPendingConnection().getAddress().toString().split(":")[0];
                ip = ip.replaceAll("/", "");

                if (GeneralConfy.isWhitelistIp()
                        && !GeneralConfy.isWhitelistUUID()) {
                    if (!WhitelistIpManagement.isWhitelist(ip)) {
                        players.get(bucle).disconnect(new TextComponent(MaintenanceManagement.kick));
                    }
                } else if (!GeneralConfy.isWhitelistIp()
                        && GeneralConfy.isWhitelistUUID()) {
                    if (!WhitelistUUIDManagement.isWhitelist(players.get(bucle).getUniqueId())) {
                        players.get(bucle).disconnect(new TextComponent(MaintenanceManagement.kick));
                    }
                } else if (GeneralConfy.isWhitelistIp()
                        && GeneralConfy.isWhitelistUUID()) {
                    if (!WhitelistIpManagement.isWhitelist(ip)
                            && !WhitelistUUIDManagement.isWhitelist(players.get(bucle).getUniqueId())) {
                        players.get(bucle).disconnect(new TextComponent(MaintenanceManagement.kick));
                    }
                } else if (!GeneralConfy.isWhitelistIp()
                        && !GeneralConfy.isWhitelistUUID()) {
                    players.get(bucle).disconnect(new TextComponent(MaintenanceManagement.kick));
                }

                bucle ++;
            }
        }
    }

    public static int getMaxPlayers() {
        return generalConfy.getInt("General.ping.max");
    }
    public static void setMaxPlayers(int max) {
        generalConfy.set("General.ping.max", max);
        GeneralConfy.saveFile(generalConfy, generalFile);
    }

    public static void saveFile(Configuration file, File fileg) {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(file, fileg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void reload() {
        try {
            generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
